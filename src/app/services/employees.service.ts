import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError, from, of } from 'rxjs';
import { endpoints } from './../shared/endpoints';
import { catchError, filter, map, switchMap } from 'rxjs/operators';
import { Employee } from './../models/employee';

interface ApiResponse {
  status: boolean;
  data: Employee[];
}

@Injectable({
  providedIn: 'root',
})
export class EmployeesService {
  constructor(private httpClient: HttpClient) {}

  getAllEmployees(): Observable<ApiResponse> {
    const endpoint = `${endpoints.employees.getDummy}`;
    return this.httpClient
      .get<ApiResponse>(endpoint)
      .pipe(catchError((err) => throwError(err)));
  }

  getEmployeeById(id: string) {
    const endpoint = `${endpoints.employees.getDummy}`;
    const source = this.httpClient
      .get<ApiResponse>(endpoint)
      .pipe(switchMap((res) => res.data))
      .pipe(catchError(() => of([])));

    return source
      .pipe(
        filter((val: Employee) => {
          return val.id === id;
        })
      )
      .pipe(catchError((err) => throwError(err)));
  }
}
