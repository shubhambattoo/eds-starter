import { Injectable } from '@angular/core';
import isEqual from 'lodash.isequal';
import { sha256 } from 'js-sha256';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private user = {
    email: 'hello@admin.com',
    password: 'test1234',
  };
  constructor() {}

  doLogin(data: { email: string; password: string }): Promise<any> {
    return new Promise((resolve, reject) => {
      if (isEqual(data, this.user)) {
        const token = sha256(data.email);
        resolve(token);
      } else {
        reject();
      }
    });
  }
}
