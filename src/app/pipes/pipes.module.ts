import { NgModule } from '@angular/core';
import { AgePipe } from './age.pipe';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [AgePipe],
  imports: [CommonModule],
  exports: [AgePipe],
})
export class PipesModule {}
