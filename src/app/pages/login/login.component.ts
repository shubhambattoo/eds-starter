import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import CustomErrorMatcher from './../../classes/ErrorStateMatcher';
import { ValidateEmail } from './../../validators/CustomEmailValidator';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  errorMatcher = new CustomErrorMatcher();
  logginIn = false;
  constructor(
    private auth: AuthService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      email: new FormControl(null, [Validators.required, ValidateEmail]),
      password: new FormControl(null, [Validators.required]),
    });
  }

  handleLogin() {
    if (!this.loginForm.valid) {
      return;
    }
    const requestObj = {
      email: this.loginForm.get('email').value,
      password: this.loginForm.get('password').value,
    };
    this.logginIn = true;
    this.auth
      .doLogin(requestObj)
      .then((token) => {
        localStorage.setItem('accessToken', token);
        this.logginIn = false;
        this.router.navigate(['/employees']);
      })
      .catch(() => {
        this.logginIn = false;
        this.openSnackBar('Something went wrong, try again', 'Ok');
      });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 3500,
    });
  }
}
