import { Component, OnInit, ViewChild } from '@angular/core';
import { EmployeesService } from './../../services/employees.service';
import { Employee } from 'src/app/models/employee';
import { MatSort, Sort } from '@angular/material/sort';
import { compare } from './../../shared/sortableCompare';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss'],
})
export class EmployeesComponent implements OnInit {
  employees: Employee[] = [];
  columns = ['id', 'name', 'salary', 'age'];
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private employeesService: EmployeesService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.getEmployees();
  }

  getEmployees(): void {
    this.employeesService.getAllEmployees().subscribe(
      (res) => {
        this.employees = res.data;
      },
      (err) => {
        this.employees = [];
      }
    );
  }

  /**
   * Sorts the current data on the table
   * @param sort sort event fired on change
   */
  sortData(sort: Sort): void {
    const data = this.employees.slice();
    if (!sort.active || sort.direction === '') {
      this.employees = data;
      return;
    }

    this.employees = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'id':
          return compare(Number(a.id), Number(b.id), isAsc);
        case 'name':
          return compare(a.employee_name, b.employee_name, isAsc);
        case 'salary':
          return compare(a.employee_salary, b.employee_salary, isAsc);
        case 'age':
          return compare(a.employee_age, b.employee_age, isAsc);
        default:
          break;
      }
    });
  }

  showDetails(row) {
    this.router.navigate(['/employee', row.id]);
  }
}
