import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeesRoutingModule } from './employees-routing.module';
import { EmployeesComponent } from './employees.component';
import { MaterialModule } from 'src/app/shared/material.module';
import { PipesModule } from 'src/app/pipes/pipes.module';


@NgModule({
  declarations: [EmployeesComponent],
  imports: [
    CommonModule,
    MaterialModule,
    PipesModule,
    EmployeesRoutingModule
  ]
})
export class EmployeesModule { }
