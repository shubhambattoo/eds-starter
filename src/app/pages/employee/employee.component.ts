import { Component, OnInit } from '@angular/core';
import { EmployeesService } from 'src/app/services/employees.service';
import { ActivatedRoute } from '@angular/router';
import { Employee } from 'src/app/models/employee';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss'],
})
export class EmployeeComponent implements OnInit {
  employee: Employee;
  constructor(
    private employeesService: EmployeesService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((data) => {
      this.getEmployee(data.id);
    });
  }

  getEmployee(id) {
    this.employeesService.getEmployeeById(id).subscribe(
      (emps) => {
        this.employee = emps;
      },
      (err) => {
        this.employee = null;
      }
    );
  }
}
