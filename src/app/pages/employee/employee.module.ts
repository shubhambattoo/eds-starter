import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeeRoutingModule } from './employee-routing.module';
import { EmployeeComponent } from './employee.component';
import { MaterialModule } from 'src/app/shared/material.module';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { DirectivesModule } from 'src/app/directives/directives.module';

@NgModule({
  declarations: [EmployeeComponent],
  imports: [
    CommonModule,
    MaterialModule,
    EmployeeRoutingModule,
    PipesModule,
    DirectivesModule,
  ],
})
export class EmployeeModule {}
