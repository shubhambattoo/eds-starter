export const endpoints = {
  employees: {
    get: 'https://dummy.restapiexample.com/api/v1/employees',
    getDummy : 'assets/dummyData/employees.json'
  },
};
