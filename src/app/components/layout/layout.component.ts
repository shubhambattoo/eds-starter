import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss'],
})
export class LayoutComponent implements OnInit {
  links = [
    { link: '/', title: 'my link' },
    { link: '/', title: 'my link' },
    { link: '/', title: 'my link' },
    { link: '/', title: 'my link' },
    { link: '/', title: 'my link' },
  ];
  constructor() {}

  ngOnInit(): void {}
}
